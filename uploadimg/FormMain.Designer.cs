﻿namespace uploadimg
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDowload = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.webView = new System.Windows.Forms.WebBrowser();
            this.btnDowload = new System.Windows.Forms.Button();
            this.txtUpload = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnGetdata = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtDowload
            // 
            this.txtDowload.Location = new System.Drawing.Point(6, 19);
            this.txtDowload.Name = "txtDowload";
            this.txtDowload.Size = new System.Drawing.Size(235, 220);
            this.txtDowload.TabIndex = 0;
            this.txtDowload.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtDowload);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(252, 249);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Web";
            // 
            // webView
            // 
            this.webView.Location = new System.Drawing.Point(282, 21);
            this.webView.MinimumSize = new System.Drawing.Size(20, 20);
            this.webView.Name = "webView";
            this.webView.Size = new System.Drawing.Size(684, 489);
            this.webView.TabIndex = 2;
            // 
            // btnDowload
            // 
            this.btnDowload.Location = new System.Drawing.Point(403, 521);
            this.btnDowload.Name = "btnDowload";
            this.btnDowload.Size = new System.Drawing.Size(75, 23);
            this.btnDowload.TabIndex = 3;
            this.btnDowload.Text = "Dowload";
            this.btnDowload.UseVisualStyleBackColor = true;
            this.btnDowload.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtUpload
            // 
            this.txtUpload.Location = new System.Drawing.Point(6, 19);
            this.txtUpload.Name = "txtUpload";
            this.txtUpload.Size = new System.Drawing.Size(235, 209);
            this.txtUpload.TabIndex = 0;
            this.txtUpload.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUpload);
            this.groupBox2.Location = new System.Drawing.Point(12, 267);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(252, 243);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sever";
            // 
            // btnGetdata
            // 
            this.btnGetdata.Location = new System.Drawing.Point(513, 521);
            this.btnGetdata.Name = "btnGetdata";
            this.btnGetdata.Size = new System.Drawing.Size(75, 23);
            this.btnGetdata.TabIndex = 4;
            this.btnGetdata.Text = "Get Data";
            this.btnGetdata.UseVisualStyleBackColor = true;
            this.btnGetdata.Click += new System.EventHandler(this.btnGetdata_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 556);
            this.Controls.Add(this.btnGetdata);
            this.Controls.Add(this.btnDowload);
            this.Controls.Add(this.webView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormMain";
            this.Text = "Form1";
            this.Shown += new System.EventHandler(this.FormMain_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.RichTextBox txtDowload;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.WebBrowser webView;
        private System.Windows.Forms.Button btnDowload;
        private System.Windows.Forms.RichTextBox txtUpload;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnGetdata;
    }
}

