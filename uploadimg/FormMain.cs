﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Web;

namespace uploadimg
{
    public partial class FormMain : Form
    {
        private int Tag = 2;
        private int INDEX = 2;
        private bool checkThread = true;
        private String Path = @"D:\Sever\htdocs\wp-admin\categories\2\";
        public FormMain()
        {
            InitializeComponent();
        }


        private void FormMain_Shown(object sender, EventArgs e)
        {

        }


        private bool UploadSever(int tag, String name)
        {
            try
            {
                WebClient client = new WebClient();
                String url = $"http://192.168.1.85/upload/" + $"?cat_id={HttpUtility.HtmlEncode(tag)}&name={HttpUtility.HtmlEncode(name)}";
                client.DownloadString(url);
                return true;
            }
            catch (Exception) { return false; }
        }

        private void DowloadImg()
        {

            try
            {
                // dow load array id 
                WebClient client = new WebClient();

                String linkRoot = @"https://www.pexels.com/vi-vn/tim-kiem/h%C3%ACnh%20n%E1%BB%81n%20thi%C3%AAn%20h%C3%A0/?";
                string content = linkRoot + $"format=js&seed=&page={INDEX}&type=";
                webView.Navigate(content);

                INDEX++;

            }
            catch (Exception e)
            {
                WriteLogWeb($"Index ={INDEX}", Color.Red);
                WriteLogWeb(e.ToString(), Color.Red);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkThread)
            {
                Thread thread = new Thread(GetImg);
                checkThread = false;
                thread.Start();
            }

        }

        private void GetImg()
        {


            HtmlDocument htmlDoc = null;

            WebClient client = new WebClient();
            String rootLink = "https://www.pexels.com";
            this.Invoke(new MethodInvoker(delegate ()
            {

                htmlDoc = webView.Document as HtmlDocument;
            }));
            String x = HttpUtility.HtmlDecode(htmlDoc.ActiveElement.InnerHtml);
            MatchCollection links = Regex.Matches(x, "rd__button href=\"(.+?)\"");

            MatchCollection Names = Regex.Matches(x, "data-big-src=.+?(.+?)auto");


            for (int i = 0; i < links.Count; i++)
            {
                String linkdowload = links[i].Groups[1].Value;
                linkdowload = rootLink + linkdowload.Remove(linkdowload.Length - 2).Substring(2);

                String[] array = Names[i].Groups[1].Value.Split('/');

                String Name = array[array.Length - 1].Replace("?", "");

               
                try
                {
                    client.DownloadFile(new Uri(linkdowload), Path + Name);
                    UploadSever(Tag, Name);
                    WriteLogWeb(Path + Name, Color.Red);
                }
                catch (Exception) { }

            }

            checkThread = true;
        }



        #region Write log Sever and Web
        private void WriteLogSever(String text, Color color)
        {
            try
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    txtUpload.Select(0, 0);
                    txtUpload.SelectionColor = color;
                    txtUpload.SelectedText = DateTime.Now.ToString("HH:mm:ss") + " : " + text + "\r\n";
                    return;
                }));
            }
            catch (Exception) { }

        }


        private void WriteLogWeb(String text, Color color)
        {
            try
            {
                this.Invoke(new MethodInvoker(delegate ()
                {
                    txtDowload.Select(0, 0);
                    txtDowload.SelectionColor = color;
                    txtDowload.SelectedText = DateTime.Now.ToString("HH:mm:ss") + " : " + text + "\r\n";
                    return;
                }));
            }
            catch (Exception) { }

        }
        #endregion

        private void btnGetdata_Click(object sender, EventArgs e)
        {
            DowloadImg();
        }
    }
}
